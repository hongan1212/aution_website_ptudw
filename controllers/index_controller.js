
const express = require('express');
const router = express.Router();

const moment = require('moment');

const mUser = require('../models/user_model');
const mCat = require('../models/category_model');
const mPro = require('../models/product_model');
const order = require('../models/order_model');
const extendsFunc = require('../utils/extensionFunc'), 
    run = extendsFunc.errorHandle,
    convertTime = extendsFunc.convertTime;

//direct of home page
router.get('/', async (req, res, next) => {
    
    //========= get data from model
    const LIMIT = 5;
    //get category
    const [categories,cErr] = await run(mCat.allCategories());
    if (cErr)
    {
        return next(cErr);
    }
    
    //get top 5 products close to end time
    const [nearEnd,nearEndErr]  =await run(mPro.getNumberofProducts(LIMIT, 'end_time'));
    if (nearEndErr)
    {
        return next(nearEndErr);
    }
    
    //get top 5 most expensive product
    const [mostExpensive,ExpensiveErr]  =await run(mPro.getNumberofProducts(LIMIT, 'price'));
    if (ExpensiveErr)
    {
        return next(ExpensiveErr);
    }
    
    //get top 5 most bidded products
    const [mostBidded,BiddedErr]  =await run(mPro.getNumberofProducts(LIMIT, 'number_of_bidded'));
    if (BiddedErr)
    {
        return next(BiddedErr);
    }
   
    //================= create side of category
    let sideCat = [];
    for (let i = 0; i < categories.length; i++) {
        sideCat[i] = {
            CatID: categories[i].CategoryID,
            CatName: categories[i].CatName,
        };
    }

    //============== start to get infor of products
    //infor of top 5 most Bidded products
    
    let mostBiddedPro = [];
    for (let i = 0 ; i < mostBidded.length ;i++)
    {
        //B1: get name of user who bidded the highest price
        const [productinfo, mbErr] = await run(order.nameofMostPriceBidder(mostBidded[i].ProID));
        //console.log(productinfo);
        if (mbErr)
        {
            return next(mbErr);
        }

        const [seller,sellerErr] = await run(mUser.getByUserID(mostBidded[i].SellerID));
        if (sellerErr)
        {
            return next(sellerErr);
        }

        //B2 : convert end time to remainingTime
        const endtime = new Date(mostBidded[i].end_time);
        const remainTime = convertTime(endtime.getTime() - Date.now());
        
        //B3: convert name 
        let name = productinfo.f_fullname;
            
        let postion = name.lastIndexOf(" ");
        name = name.slice(0,postion);
        productinfo.f_fullname = productinfo.f_fullname.replace(name, '*******');

        let sname = seller.f_fullname;
        let spostion = sname.lastIndexOf(" ");
        sname = sname.slice(0,spostion);
        seller.f_fullname = seller.f_fullname.replace(sname, '*******');
       
       //B3: Create a object contain data to render 
        mostBiddedPro[i] = {
            ProID: mostBidded[i].ProID,
            ProName: mostBidded[i].ProName,
            Price: productinfo.maxprice,
            Name: productinfo.f_fullname,
            ceilPrice: mostBidded[i].CeilPrice,
            //using toLocateDateString to convert international time to local time string 
            Post: mostBidded[i].create_time.toLocaleDateString(),
            End: remainTime,
            number_of_bidded: mostBidded[i].number_of_bidded,
            NameSeller: seller.f_fullname,
        };
        //console.log(mostBiddedPro[i]);
    }

    //infor of top 5 most Expensive products
    let mostExpPro = [];

    for (let i = 0 ; i < mostExpensive.length ;i++)
    {
        //B1: get name of user who bidded the highest price
        const [productinfo, mbErr] = await run(order.nameofMostPriceBidder(mostExpensive[i].ProID));
        if (mbErr)
        {
            return next(mbErr);
        }

        const [seller,sellerErr] = await run(mUser.getByUserID(mostExpensive[i].SellerID));
        if (sellerErr)
        {
            return next(sellerErr);
        }

        //B2 : convert end time to remainingTime
        const endtime = new Date(mostBidded[i].end_time);
        const remainTime = convertTime(endtime.getTime() - Date.now());

        //B3: convert name 
        let name = productinfo.f_fullname;
            
        let postion = name.lastIndexOf(" ");
        name = name.slice(0,postion);
        productinfo.f_fullname = productinfo.f_fullname.replace(name, '*******');

        let sname = seller.f_fullname;
        let spostion = sname.lastIndexOf(" ");
        sname = sname.slice(0,spostion);
        seller.f_fullname = seller.f_fullname.replace(sname, '*******');

       //B3: Create a object contain data to render 
        mostExpPro[i] = {
            ProID: mostExpensive[i].ProID,
            ProName: mostExpensive[i].ProName,
            Price: productinfo.maxprice,
            Name: productinfo.f_fullname,
            ceilPrice: mostExpensive[i].CeilPrice,
            //using toLocateDateString to convert international time to local time string 
            Post: mostExpensive[i].create_time.toLocaleDateString(),
            End: remainTime,
            number_of_bidded: mostExpensive[i].number_of_bidded,
            NameSeller: seller.f_fullname,
        };
        
        //console.log(mostExpPro[i]);
    }

    //infor of top 5 colse to end time products
    let nearEndPro = [];

    for (let i = 0 ; i < nearEnd.length ;i++)
    {
        //B1: get name of user who bidded the highest price
        const [productinfo, mbErr] = await run(order.nameofMostPriceBidder(nearEnd[i].ProID));
        if (mbErr)
        {
            return next(mbErr);
        }

        const [seller,sellerErr] = await run(mUser.getByUserID(nearEnd[i].SellerID));
        if (sellerErr)
        {
            return next(sellerErr);
        }

        //B2 : convert end time to remainingTime
        const endtime = new Date(mostBidded[i].end_time);
        const remainTime = convertTime(endtime.getTime() - Date.now());

        //B3: convert name 
        let name = productinfo.f_fullname;
        let postion = name.lastIndexOf(" ");
        name = name.slice(0,postion);
        productinfo.f_fullname = productinfo.f_fullname.replace(name, '*******');

        let sname = seller.f_fullname;
        let spostion = sname.lastIndexOf(" ");
        sname = sname.slice(0,spostion);
        seller.f_fullname = seller.f_fullname.replace(sname, '*******');

       //B4: Create a object contain data to render 
        nearEndPro[i] = {
            ProID: nearEnd[i].ProID,
            ProName: nearEnd[i].ProName,
            Price: productinfo.maxprice,
            Name: productinfo.f_fullname,
            ceilPrice: nearEnd[i].CeilPrice,
            //using toLocateDateString to convert international time to local time string 
            Post: nearEnd[i].create_time.toLocaleDateString(),
            End: remainTime,
            number_of_bidded: nearEnd[i].number_of_bidded,
            NameSeller: seller.f_fullname,
        };
        
        //console.log(mostExpPro[i]);
    }

    res.render('home', {
        title: 'Online Auction',
        sideCategory: sideCat,
        mostBiddedPro,
        mostExpPro,
        nearEndPro,
    });
});

//contact page
router.get('/contact', (req, res) => {
    res.render('./layouts/contact', {
        title: 'Contact us',
        layout: 'contact',
    });
});

//about page
router.get('/about', (req, res) => {
    res.render('./layouts/about', {
        title: 'About us',
        layout: 'about',
    });
});

module.exports = router;